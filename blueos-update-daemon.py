#!/usr/bin/python3
import urllib.request
import os
import os.path
import time

if not os.path.exists(os.path.expanduser("~/.panelconfig")):
    os.system("xfpanel-switch load /usr/share/panel.conf.tar.bz2")
    os.system("touch ~/.panelconfig")

while True:
    url = "https://updates.koyu.space/blueos/latest"
    response = urllib.request.urlopen(url)
    data = response.read()
    latest = data.decode("utf-8").split("\n")[0]
    f = open("/usr/share/blueos-release", "r")
    s = f.read().split("\n")[0]
    f.close()
    if s != latest:
        os.system("notify-send \"BlueOS system notification\" \"New update available. Please launch the BlueOS Upgrader.\"")
    time.sleep(7200)